package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateKontoklasse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateKontoklasseRepository extends JpaRepository<TemplateKontoklasse, Long> {
}
