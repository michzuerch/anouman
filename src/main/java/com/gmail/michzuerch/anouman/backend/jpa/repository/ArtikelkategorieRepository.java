package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Artikelkategorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtikelkategorieRepository extends JpaRepository<Artikelkategorie, Long> {
}
