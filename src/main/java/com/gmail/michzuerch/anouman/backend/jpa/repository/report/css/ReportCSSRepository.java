package com.gmail.michzuerch.anouman.backend.jpa.repository.report.css;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.css.ReportCSS;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportCSSRepository extends JpaRepository<ReportCSS, Long> {
}
