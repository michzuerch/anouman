package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.Aufwand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AufwandRepository extends JpaRepository<Aufwand, Long> {
}
