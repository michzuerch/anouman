package com.gmail.michzuerch.anouman.backend.jpa.repository.report.fop;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.fop.ReportFOP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportFOPRepository extends JpaRepository<ReportFOP, Long> {
}
