package com.gmail.michzuerch.anouman.backend.jpa.repository.report.jasper;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.jasper.ReportJasper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportJasperRepository extends JpaRepository<ReportJasper, Long> {
}
