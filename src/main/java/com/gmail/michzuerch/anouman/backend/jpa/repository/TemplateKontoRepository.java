package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.TemplateKonto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateKontoRepository extends JpaRepository<TemplateKonto, Long> {
}
