package com.gmail.michzuerch.anouman.backend.jpa.repository.report.fop;

import com.gmail.michzuerch.anouman.backend.jpa.domain.report.fop.ReportFOPImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportFOPImageRepository extends JpaRepository<ReportFOPImage, Long> {
}
