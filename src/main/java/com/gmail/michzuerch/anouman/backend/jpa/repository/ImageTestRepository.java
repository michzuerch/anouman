package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.ImageTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageTestRepository extends JpaRepository<ImageTest, Long> {
}
