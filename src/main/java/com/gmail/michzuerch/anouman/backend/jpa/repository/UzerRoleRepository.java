package com.gmail.michzuerch.anouman.backend.jpa.repository;

import com.gmail.michzuerch.anouman.backend.jpa.domain.UzerRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UzerRoleRepository extends JpaRepository<UzerRole, Long> {
}
